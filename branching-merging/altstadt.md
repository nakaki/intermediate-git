
# Altstadt

### Soban

Zwingerstr. 21, Heidleberg, BW 69117
[Soban](https://www.facebook.com/pages/Soban-2/578112785591936) 

#### Pro

- good, spicy Korean food

#### Con

- not many vegetarian/vegan options
- quite small - best to make a reservation!
