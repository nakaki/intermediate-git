# Barcelona

### Cerveceria Catalana

Carrer de Mallorca, 236, 08008, Barcelona
[Web](https://www.facebook.com/pages/category/Tapas-Bar---Restaurant/Cervecer%C3%ADa-Catalana-539478542770052/)

#### Pro

- good tapas. one of the best tapas bar in Barcelona
- Not the cheapest option but reasonable prices

#### Con

- always busy 
- not allowed to  make a reservation - you have to go early
